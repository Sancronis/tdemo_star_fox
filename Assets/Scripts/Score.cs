﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Score : MonoBehaviour
{
    public static float score;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI enemiesKilledText;
    public static int enemyCount;
    public static int enemiesKilled;

    public static Spawn spawn;
    public GameObject spawnGameObject;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        enemyCount = 0;
        enemiesKilled = 0;
        spawn = spawnGameObject.GetComponent<Spawn>();
    }





    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime * 10;
        scoreText.text = (score + "m");
        enemiesKilledText.text = (enemiesKilled + " Hostiles Killed");
    }

    public static void ChangeEnemyCount(int countChange)
    {
        if (countChange == -1)
        {
            enemiesKilled++;
        }
        enemyCount += countChange;
        if (enemyCount == 10)
        {
            spawn.playerDeath();
        }
    }
}
