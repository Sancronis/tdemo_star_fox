﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private int LocationCheck = 1;
    private int LocationCheckY = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(LocationCheck == 1){
        transform.Translate(8 * Time.deltaTime, 0, 0);
        }
        else if(LocationCheck == 2){
        transform.Translate(-8 * Time.deltaTime, 0, 0);
        }

        if (LocationCheckY == 1)
        {
            transform.Translate(0, 3 * Time.deltaTime, 0);
        }
        else if (LocationCheckY == 2)
        {
            transform.Translate(0, -3 * Time.deltaTime, 0);
        }

        if (transform.position.x <= -20){
            LocationCheck = 1;
        }

        else if(transform.position.x >= 20)
        {
            LocationCheck = 2;
        }

        if (transform.position.y <= 0)
        {
            LocationCheckY = 1;
        }

        else if (transform.position.y >= 10)
        {
            LocationCheckY = 2;
        }
    }
}
