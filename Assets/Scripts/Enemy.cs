﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 2f;
    public float currentHealth;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damageRecieved)
    {
        currentHealth -= damageRecieved;
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Score.ChangeEnemyCount(-1);
        Destroy(gameObject);
    }
}
