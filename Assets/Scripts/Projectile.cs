﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public string element;
    //collision logic yadadadada
    public ProjectileController projectileController;

    // Start is called before the first frame update
    void Start()
    {
//        projectileController = GameObject.Find("Cube").GetComponent<ProjectileController>(); //need to change if player asset name gets changed
        }

    // Update is called once per frame
    void Update()
    {

    }
    //    private void OnTriggerEnter(Collider collision)

    private void OnCollisionEnter(Collision collision)
//    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.GetComponent<Projectile>() || collision.gameObject.tag == "Obstacle")
        {
            return;
        }
        else if (collision.gameObject.tag != null) //check it is an enemy layer with a tag
        {
            float damageDealt = projectileController.DamageCalculation(element, collision.gameObject.tag);
            if (damageDealt > 0)
            {
                collision.gameObject.GetComponent<Enemy>().TakeDamage(damageDealt);
                Debug.Log(damageDealt + " : " + collision.gameObject.name);
                Destroy(gameObject);
            }

        }
    }
}
