﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public int ObstacleNumber;
    private static float distance;


    void Start()
    {
        ObstacleNumber = 0;
    }

    void spawnObstacle()
    {
        Vector3 position = new Vector3(Random.Range(-20, 40), -4, Random.Range(200, 300));
        Instantiate(gameObject, position, Quaternion.identity);
    }

    void Update()
    {
//        distance = Score.score;
//        print(distance);
        //        Vector3 position = new Vector3(Random.Range(-20,20),0,Random.Range(200,300));

        transform.Translate(0, 0, -45 * Time.deltaTime);  //adjust the third value for faster moving obstacles.

        if (transform.position.z < -50)
        {
            //         Instantiate(gameObject, position, Quaternion.identity);
            spawnObstacle();
            Destroy(this.gameObject);

        }
    }
}



