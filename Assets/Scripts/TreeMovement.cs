﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeMovement : MonoBehaviour
{
    public int ObstacleNumber;   //This is just a clone of Obstaclemovement but going a different direction.
    private static float distance;

    public bool isOak;
    public bool isPine;
    public bool isMushroom;

    public Transform treeObject;

    void Start()
    {
        ObstacleNumber = 0;
    }

    void spawnObstacle()
    {
        Vector3 position = new Vector3(Random.Range(-20, 20), 3, Random.Range(200, 300)); //Default to oak position then reassign if necessary
//        if (isOak) { position = new Vector3(Random.Range(-20, 20), 5, Random.Range(200, 300)); }
        if (isPine) { position = new Vector3(Random.Range(-20, 20), 1, Random.Range(200, 300)); }
        if (isMushroom) { position = new Vector3(Random.Range(-20, 20), -4, Random.Range(200, 300)); }
        Instantiate(gameObject, position, Quaternion.identity);
        //This is so messy but not sure what else to do. Not happy with it
//        treeObject.transform.eulerAngles = new Vector3(-89.98f, 0.0f, 0.0f);
    }

    void Update()
    {
//        distance = Score.score;
//        print(distance);
        //        Vector3 position = new Vector3(Random.Range(-20,20),0,Random.Range(200,300));


        transform.Translate(0, 45  * Time.deltaTime, -0);  //adjust the third value for faster moving obstacles.

        if (transform.position.z < -50)
        {
            //         Instantiate(gameObject, position, Quaternion.identity);
            spawnObstacle();
            Destroy(this.gameObject);

        }
        treeObject.transform.eulerAngles = new Vector3(-89.98f, 0.0f, 0.0f);
//        treeObject.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
    }
}



