﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Spawn : MonoBehaviour
{
    float spawn = 25.0f;
    float timer = 0.0f;
    private static float distance;
    public GameObject enemy;
    public bool dead;
    public float deathTime = 0.0f;
    public GameObject player;
    public AudioSource deathSound;

    void Update()
    {
        distance = Score.score; //Sets the distance gone using the score
        timer += Time.deltaTime; //Creates a timer for spawn rate
        Vector3 position = new Vector3(Random.Range(-50,-20),0,Random.Range(20,40));
        if(distance >= 50){    //How far player goes before spawning these enemies
        if(timer >= spawn){
             Instantiate(enemy, position, Quaternion.identity);
             Score.ChangeEnemyCount(1);
             timer = 0.0f;
        }
        //Intended to only have one enemy at a time
        //This wasn't going as intended but adds the difficulty on not defeating enemies then more will spawn over time

        }
        if (dead && Time.time > deathTime+2.0f) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }

    public void playerDeath()
    {
        deathSound.Play();
        dead = true;
        deathTime = Time.time;
        Destroy(player);
    }
}
