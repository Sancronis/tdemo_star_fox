﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSpawn : MonoBehaviour
{
    float spawn = 15.0f;
    float timer = 0.0f;
    private static float distance;
    public GameObject enemy;

    void Update()
    {
        distance = Score.score;
        timer += Time.deltaTime;
        Vector3 position = new Vector3(Random.Range(-50,-20),0,Random.Range(20,40));
        if(distance >= 400){
        if(timer >= spawn){
             Instantiate(enemy, position, Quaternion.identity);
                Score.ChangeEnemyCount(1);
                timer = 0.0f;
        }

    }
}
}
