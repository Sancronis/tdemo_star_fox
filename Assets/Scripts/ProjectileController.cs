﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectileController : MonoBehaviour
{
    public GameObject waterPrefab;
    public GameObject firePrefab;
    public GameObject lightningPrefab;
    GameObject shootPoint;
    Element water;
    Element fire;
    Element lightning;
    Element currentElement;
    public Image fireImage;
    public Image waterImage;
    public Image lightningImage;

    public AudioSource lightningAttack;
    public AudioSource fireAttack;
    public AudioSource waterAttack;
    public AudioSource fireImpact;
    public AudioSource lightningImpact;
    public AudioSource waterImpact;
    public Spawn spawn;

    public string currentSound;

    // Start is called before the first frame update
    void Start()
    {
        water = new Element { baseDamage = 1, prefab = waterPrefab, image = waterImage };
        fire = new Element { baseDamage = 1, prefab = firePrefab, image = fireImage };
        lightning = new Element { baseDamage = 1, prefab = lightningPrefab, image = lightningImage };
        shootPoint = GameObject.Find("ShootPoint");
        currentElement = water;
        fireImage.enabled = false;
        lightningImage.enabled = false;
        waterImage.enabled = true;
        currentSound = "water";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwapElement(water);
            currentSound = "water";
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwapElement(fire);
            currentSound = "fire";
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SwapElement(lightning);
            currentSound = "lightning";
        }
        if (Input.GetMouseButtonDown(0))
        {
            switch (currentSound)
            {
                case "fire":
                    fireAttack.Play();
                    break;
                case "lightning":
                    lightningAttack.Play();
                    break;
                default:
                    waterAttack.Play();
                    break;
            }
                    GameObject projectile = Instantiate(currentElement.prefab);
            projectile.GetComponent<Projectile>().projectileController = this;
            projectile.transform.position = shootPoint.transform.position;
            //            projectile.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * 400;
            Vector3 mouse = Input.mousePosition;
            Vector3 myScreenPos = Camera.main.WorldToScreenPoint(transform.position);
            projectile.GetComponent<Rigidbody>().velocity =  Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 200));
            StartCoroutine(ProjectileDespawn(projectile));
        }
    }

    IEnumerator ProjectileDespawn(GameObject projectile)
    {
        yield return new WaitForSeconds(2);
        Destroy(projectile);
    }

    void SwapElement(Element newElement)
    {
        if (currentElement != newElement)
        {
            currentElement.image.enabled = false;
            currentElement = newElement;
            currentElement.image.enabled = true;
        }
        Debug.Log("currently shooting:" + currentElement.prefab.name);
    }

    public float DamageCalculation(string projectileElement, string enemyElement)
    {
        enemyElement = enemyElement.ToLower();
        projectileElement = projectileElement.ToLower();
        if (enemyElement == "untagged")
        {
            return 0;
        }
        else
        {
                switch (projectileElement)
            {
                case "water":
                    waterImpact.Play();
                    if (enemyElement == "fire") { return water.baseDamage * 2; }
                    else if (enemyElement == "lightning") { return 0; }
                    else { return water.baseDamage; }
                case "fire":
                    fireImpact.Play();
                    if (enemyElement == "lightning") { return fire.baseDamage * 2; }
                    else if (enemyElement == "water") { return 0; }
                    else { return fire.baseDamage; }
                case "lightning":
                    lightningImpact.Play();
                    if (enemyElement == "water") { return lightning.baseDamage * 2; }
                    else if (enemyElement == "fire") { return 0; }
                    else { return lightning.baseDamage; }
                default:
                    return 0;
            }
        }
        
    }
}
public class Element
{
    public GameObject prefab;
    public float baseDamage;
    public Image image;
}


